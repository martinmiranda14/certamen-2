import express, { Express } from 'express'
import bodyParser from 'body-parser'
import { createConnection } from "typeorm"

// Controllers
import * as rootController from './controller/rootController'
import * as moviesController from './controller/moviesController'
import * as  userController from './controller/userController'

const server: Express = express()

server.use(bodyParser.json())
server.use(bodyParser.urlencoded({
    extended: true
}))


// DB

/* Conexion DB y APP*/
createConnection().then(
    () => console.log('Conexión creada')
).catch(
    error => console.log('Error en la conexión', error)
)


// Services
server.get('/', rootController.sendDeafultMessage)
server.get('/movies', moviesController.sendMoviesInfo)

/* Pregunta 1 */
server.post('/user',userController.createUser)

/* Pregunta 2 */
server.delete('/user/:rut',userController.deleteUser)




server.listen(3000, () => {
    console.log('Server listening at port 3000')
})