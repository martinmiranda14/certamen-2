import "reflect-metadata"
import { getRepository } from "typeorm"

import { User } from "../model/User"

import {formatoRut,digitoVerificador,formatoCorreo} from "../funciones/validateUser"



/**
 * Funcion createUser ()
 * @param rut 
 * @param nombre 
 * @param edad 
 * @param correoElectronico 
 * @param generosFavoritos 
 * 
 * @returns "code":200 ; Usuario se crea correctamente
 * @returns "code":500; Usuario no se crea correctamente
 */
export const createUser = async (rut:string , nombre:string , edad:Number ,correoElectronico:string,generosFavoritos:string[])=>{
    const user =new User()
    /* Datos no vacios (los que no son ni rut ni correo) */
    
    if(nombre.length==0){
        return {
            "code":500
        }
    }
    if(!edad){
        return {
            "code":500
        }
    }
    if(edad<0){ /// Edad Positiva
        return {
            "code":500
        }
    }
    if(generosFavoritos.length==0){
        return {
            "code":500
        }
    }
    
    

    /* Verificar Correo */

    if(!formatoCorreo(correoElectronico)){
        return {
            "code":500
        }
    }
    

    /* Verificar rut */

    if(!(formatoRut(rut))){
        return {
            "code":500
        }
    }
    
    if (!digitoVerificador(rut)){
        return {
            "code":500
        }
    }


    /* Verificar usuario ya registrado */
    const usReg:User = await getRepository(User).findOne(rut) // Veo si el usuario ya existe
    if(usReg){ // Si existe
        return {
            "code":500 // error por usuario ya registrado
        }
    }

    /* Agregar usuario a la base de datos */


    user.rut=rut.toLowerCase()
    user.nombre=nombre
    user.edad=Number(edad)
    user.correoElectronico=correoElectronico.toLowerCase()
    user.generosFavoritos=generosFavoritos.toString()

    await getRepository(User).save(user)
    return {
        "code":200
    }

}

/** Delete User
 * 
 * @param rut 
 * 
 * @returns code:500; error deleting user
 * @returns code:400; user not foudn
 * @returns code:200; user deleted correctly
 */
export const deleteUser= async (rut:string) =>{
    /* Verificar rut */
    
    if(!(formatoRut(rut))){
        return {
            "code":500
        }
    }
    
    if (!digitoVerificador(rut)){
        return {
            "code":500
        }
    }
    const user:User= await getRepository(User).findOne(rut)
    /* User not found */
    if(!user){
        return{
            "code":400
        }
    }

    /* User found */
    await getRepository(User).remove(user)
    /* Corroborar usuario borrado */
    const userCorr:User= await getRepository(User).findOne(rut)
    /* User not found */
    if(userCorr){
        return{
            "code":500
        }
    }
    /*Retorno usuario borrado correctamente */
    return{
        "code":200
    }

    
}