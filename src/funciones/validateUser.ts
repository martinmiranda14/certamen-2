/**
 * Formao Rut
 * @param rut 
 * 
 * @returns true , Rut en formato valido
 */
export const formatoRut= ( rut:string )=>{
    rut=rut.toString()
    if(!rut){
        return false
    }
    const arrayRut:string[]= rut.split('-')

    if (arrayRut.length !=2){
        return false
    }
    if(!arrayRut[0]){
        return false
    }
    if(!arrayRut[1]){
        return false
    }
    return true
}
/** Digito verificador
 * @param rut
 * 
 * @returns true ; Digito verificador correcto
 * @returns false; Digito verificador no corresponde
 */
export const digitoVerificador=  (rut:string)=>{
    rut=rut.toString()
    if(!rut){
        return false
    }
    const arrayRut:string[]= rut.split('-')
    let sum=0
    let mul=2
    const numeros=arrayRut[0]
    
    for (let i:number =0;i<numeros.length;i++){
        let number= Number(numeros[numeros.length-1-i])
        sum=sum+number*mul        
        mul=mul+1
        if(mul==8){
            mul=2
        }
    }
    let ver:string=(11-sum%11).toString()
    if(ver=='10'){
        ver="k"
    }
    if(ver=='11'){
        ver='0'
    }
    


    return ver==arrayRut[1].toLowerCase()
}

export const formatoCorreo = (correo:string)=>{
    if(!correo){
        return false
    }
    
    
    const arrCorreo:string[]=correo.split("@")
    if(arrCorreo.length !=2 ){
        return false
    }
    if(arrCorreo[0].length<1){
        return false
    }
    if(arrCorreo[0].split(/ /g).length>=2){
        return false
    }
    if(arrCorreo[1].split(/ /g).length>=2){
        return false
    }
    const dominio:string=arrCorreo[1]
    const sepDominios:string[]=dominio.split('.')
    if(sepDominios.length <2){
        return false
    }

    
    return true
}