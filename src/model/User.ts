import { Entity , Column, PrimaryColumn} from 'typeorm'

@Entity()
export class User{
    @PrimaryColumn()
    rut:string
    @Column()
    nombre:string
    @Column()
    edad:Number
    @Column()
    correoElectronico:string
    @Column()
    generosFavoritos:string



}


