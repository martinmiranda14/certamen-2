import { Request, Response } from 'express'
import * as userRepository from '../repository/userRepository'

export const createUser= async (request:Request,response:Response)=>{
    
    const result= await userRepository.createUser(request.body.rut,request.body.nombre,request.body.edad,request.body.correoElectronico,request.body.generosFavoritos)
  
    if(result.code==200){
        response.status(200).json({
            "status":"Ok",
            "message":"User created correctly",
            "userID":request.body.rut
        })

    }
    else{
        response.status(500).json({
            "status":"NOK",
            "message":"these was an error created user"
        })
    }
}
export const deleteUser=async (request:Request,response:Response)=>{

    const result= await userRepository.deleteUser(request.params.rut)
    if(result.code==200){
        response.status(200).json({
            "status":"OK",
            "message":"User deleted correctly"
        })
    }
    else if(result.code==400){
        response.status(400).json({
            "status":"NOK",
            "message":"User not found"
        })

    }
    else{
        response.status(500).json({
            "status":"NOK",
            "message":"There was an error deleting user"
        })
        
    }
}