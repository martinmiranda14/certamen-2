## Instrucciones de instalación

```
npm install
```

### Instalación de Typescript

```
npm install -g typescript
```

### Instalación de jest

```
npm install -g jest
```

## Levantando el proyecto

```
npm start
```

## Ejecutándo los tests

```
npm run test
```

### Levantar DB en  Docker (Uso de mysql para RaspberryPI)

```
docker-compose up -d
```

### Preguntas Realizadas 

* 1)
* 2)

#### Observaciones A Considerar:

* No se utiliza docker para levantar directamente node , aunque si hay codigo en el Dockerfile pero no ejecutado

**Pregunta1**
* Formato esperado json
{
    "rut":"19015217-0",
    "nombre":"Martin",
    "edad":25
    "correoElectronico":"martin@gmail.com",
    "generosFavoritos":["accion","Comedia"]

}

 * Rut se usa como clave primaria
 * En carpeca src/funciones archivo validateUser estan las funciones que validan los datos ingresados del usuario 
 * Carpeta test archivo validateUser.unit.test.js estan los test de las funciones que validan los datos de usuarios , para ejecutar test de hace con el comando 

 ```
 tsc & jest test/validateUser.test

 ```

 * Modelo User en carpeta model no puede recibir arreglos , por uso de mysql
 * No se realizan test de integracion

**Pregunta 2**
* No se realizan test unitarios puesto que ya estan realziados para la funcion validar rut
* si un rut se ingresa en un formato invalido se considerar error 500
* No se realizan test de integracion
* forma de pasar el usario a eliminar es con metodo DELETE y en la url:


```
/user/rut_usuario

```






