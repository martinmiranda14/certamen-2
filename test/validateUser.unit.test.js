
const validateuser = require('../build/funciones/validateUser.js')
/**
 * Prueba Rut Formato Valido
 * 1) Rut Valido  True
 * 2) Rut Sin Verificador  False
 * 3) Numero Vacio False
 * 4) Verificador Vacio False
 * 5) Los 2 vacios , False
 * 6) No se ingresa Rut
 * 7) Se ingresa en formato Numerico 2 Lados: True ??
 */

describe('Probando Rut en Formato Valido',()=>{
    it('Rut Valido 19015217-0',()=>{
        const result = validateuser.formatoRut('19015217-0')
        expect(result).toBe(true)
    })
    it('Rut Sin Verificador (19015217)',()=>{
        const result=  validateuser.formatoRut('19015217')
        expect(result).toBe(false)
    })
    it('Numero Vacio (-0)',()=>{
        const result=  validateuser.formatoRut('-0')
        expect(result).toBe(false)
    })
    it('Verificador Vacio (19015217-)',()=>{
        const result=  validateuser.formatoRut('19015217-')
        expect(result).toBe(false)
    })
    it('Los 2 Vacios (-)',()=>{
        const result= validateuser.formatoRut('-')
        expect(result).toBe(false)
    })
    it('No se ingresa Rut ()',()=>{
        const result= validateuser.formatoRut('')
        expect(result).toBe(false)
    })
    it('Ingreso 2 en formato numerico (19015217-0)',()=>{
        const result= validateuser.formatoRut(19015217-0)
        expect(result).toBe(false)
    })
})
/** Pruebas Digito Verificador y letras en ves de numero 
 * 1) Rut Valido con Numero
 * 2) Rtut valido con Letra * 
 * 3) Rut Invalido con Numero
 * 4) Rut Invalido con Letra
 * 5) Verificador mayor a 10
 * 6) Letra verificador distinta a k
 * 7) Letra ente sector de numeros 
 * 8) Rut valido con letra Mayuscula
 * 9) Rut con Puntos 
 */
describe('Prueba Verificador Rut ',()=>{
    it('Rut Valido con numero ("19015217-0")',()=>{
        const result = validateuser.digitoVerificador('19015217-0')
        expect(result).toBe(true)
    })
    it('Rut Valido con numero ("9939873-6")',()=>{
        const result = validateuser.digitoVerificador('9939873-6')
        expect(result).toBe(true)
    })
    it('Rut Valido con Letra ("17679304-k")',()=>{
        const result = validateuser.digitoVerificador("17679304-k")
        expect(result).toBe(true)
    })
    it('Rut Invalido con numero ("19015217-6")',()=>{
        const result = validateuser.digitoVerificador('19015217-6')
        expect(result).toBe(false)
    })
    it('Rut Invalido con Letra ("17.679.304-8")',()=>{
        const result = validateuser.digitoVerificador("17.679.304-8")
        expect(result).toBe(false)
    })
    it('Verificador mayor a 10 ("19015217-15")',()=>{
        const result = validateuser.digitoVerificador("19015217-15")
        expect(result).toBe(false)
    })
    it('Rut Invalido con Letra ("17679304-m")',()=>{
        const result = validateuser.digitoVerificador("17679304-m")
        expect(result).toBe(false)
    })
    it('Letra entre sector de numeros ("190j5217-0")',()=>{
        const result = validateuser.digitoVerificador("190j5217-0")
        expect(result).toBe(false)
    })
    it('Rut Valido con Letra Mayuscula ("17679304-K")',()=>{
        const result = validateuser.digitoVerificador("17679304-K")
        expect(result).toBe(true)
    })
    it('Rut C con puntos ("17.679.304-k"), Invalido',()=>{
        const result = validateuser.digitoVerificador("17.679.304-k")
        expect(result).toBe(false)
    })
})

/** Prueba correo de formato valido
 * 1) Correo valido martinmiranda800@gmail.com; true
 * 2) Correo valido martin.miranda.14@sansano.usm.cl; true
 * 3) Correo sin @ martinmirandaagmail.com ; false
 * 4) correo sin primera parte @gmail.com ; false
 * 5) Correo sin segunda Parte martinmiranda82@ ;false
 * 6) Correo no ingresado ;false
 * 7) Correo con espacio en primera parte ; false
 * 8) Correo con espacio en segunda parte ; false
 * 9) correo con espacio en las 2 partes ; false
 * 10) correo con Mauscula ; true (Se debe almacenar todo en minusculas si )
 */
describe('Prueba formato correo',()=>{
    it('Correo valido martinmiranda800@gmail.com ',()=>{
        const result = validateuser.formatoCorreo("martinmiranda800@gmail.com")
        expect(result).toBe(true)
    })
    it('Correo valido martin.miranda.14@sansano.usm.cl ',()=>{
        const result= validateuser.formatoCorreo("martin.miranda.14@sansano.usm.cl")
        expect(result).toBe(true)
    })
    it('Correo sin @ ',()=>{
        const result= validateuser.formatoCorreo("martinmirandaagmail.com")
        expect(result).toBe(false)
    })
    it('Correo sin primera parte ',()=>{
        const result= validateuser.formatoCorreo("@gmail.com")
        expect(result).toBe(false)
    })
    it('Correo sin segunda parte ',()=>{
        const result= validateuser.formatoCorreo("martinmiranda82@")
        expect(result).toBe(false)
    })
    it('Correo no ingresado',()=>{
        const result= validateuser.formatoCorreo()
        expect(result).toBe(false)
    })
    it('Correo con espacio en primera parte',()=>{
        const result= validateuser.formatoCorreo("martin miranda@sansano.usm.cl")
        expect(result).toBe(false)
    })
    it('Correo con espacio en segunda parte',()=>{
        const result= validateuser.formatoCorreo("martinmiranda@sansano usm.cl")
        expect(result).toBe(false)
    })
    it('Correo con espacio las 2 partes',()=>{
        const result= validateuser.formatoCorreo("martin miranda@sansano usm.cl")
        expect(result).toBe(false)
    })
    it('Correo con mayuscula',()=>{
        const result= validateuser.formatoCorreo("Martinmiranda@sansanousm.cl")
        expect(result).toBe(true)
    })
})